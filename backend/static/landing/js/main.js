document.addEventListener('DOMContentLoaded', function () {
    const swiper = new Swiper('.swiper', {
        loop: true,
        effect: 'fade',
        speed: 1500,
        navigation: {
            nextEl: '.slider-controls--next',
            prevEl: '.slider-controls--prev',
        },
        mousewheel: {
            releaseOnEdges: true,
        },

    });

    swiper.on('slideChangeTransitionStart', function () {

        anime({
            targets: '.slide_image img',
            scale: [1.2, 1],
            opacity: [0, 1],
            delay: anime.stagger(200, { start: 300, from: 'last' }),
            easing: 'easeInOutQuart'
        });
        anime({
            targets: '.swiper-slide-active .slide_text .element-anime',
            translateY: [50, 0],
            opacity: [0, 1],
            delay: anime.stagger(150, { start: 500 }),
            easing: 'easeInOutQuart'
        });
    });
    anime({
        targets: '.logo img',
        rotate: 360,
        opacity: [0, 1],
        delay: 300,
        easing: 'easeInOutQuart'
    });
    anime({
        targets: '.slide_image img',
        scale: [1.2, 1],
        opacity: [0, 1],
        delay: anime.stagger(200, { start: 1300, from: 'last' }),
        easing: 'easeInOutQuart'
    });
    anime({
        targets: '.swiper-slide-active .slide_text .element-anime',
        translateY: [50, 0],
        opacity: [0, 1],
        delay: anime.stagger(250, { start: 800 }),
        easing: 'easeInOutQuart'
    });
    anime({
        targets: '.slider-controls',
        opacity: [0, 1],
        delay: 1500,
        easing: 'easeInOutQuart'
    });
    anime({
        targets: '.slider-controls--prev',
        translateX: [-100, 0],
        opacity: [0, 1],
        delay: 1600,
        easing: 'easeInOutQuart'
    });
    anime({
        targets: '.slider-controls--next',
        translateX: [100, 0],
        opacity: [0, 1],
        delay: 1600,
        easing: 'easeInOutQuart'
    });
})
