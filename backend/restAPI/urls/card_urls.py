from django.urls import path
from restAPI.views import card_views as views


urlpatterns = [
    path('category/<str:category>/', views.userCategoryCards,
         name="user-category-cards"),
    path('create/', views.createCard, name="create-card"),
    path('delete/<str:pk>/', views.deleteCard, name="delete-card"),
    path('update/<str:pk>/', views.updateCard, name="update-card"),
]
