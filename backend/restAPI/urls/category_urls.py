from django.urls import path
from restAPI.views import category_views as views


urlpatterns = [
    path('', views.userCategories, name="user-categories"),
    path('create/', views.createCategory, name="create-category"),
    path('delete/<str:pk>/', views.deleteCategory, name="delete-category"),
    path('update/<str:pk>/', views.updateCategory, name="update-category"),
]
