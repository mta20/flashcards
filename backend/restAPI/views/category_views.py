from django.contrib.auth.models import User
from rest_framework.response import Response
from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import IsAuthenticated, IsAdminUser
from restAPI.models import Category
from restAPI.serializers import CategorySerializer
from rest_framework import status


@api_view(['GET'])
@permission_classes([IsAuthenticated])
def userCategories(request, *args, **kwargs):
    user = request.user
    categories = Category.objects.filter(user=user.id)
    serializer = CategorySerializer(categories, many=True)
    return Response(serializer.data)


@api_view(['POST'])
@permission_classes([IsAuthenticated])
def createCategory(request, *args, **kwargs):
    data = request.data
    user = request.user

    try:
        category = Category.objects.create(
            name=data['name'],
            description=data['description'],
            user=user,
        )

        serializer = CategorySerializer(category, many=False)
        return Response(serializer.data)
    except Exception as e:
        message = {
            'detail': 'err'
        }
        return Response(message, status=status.HTTP_400_BAD_REQUEST)


@api_view(['DELETE'])
@permission_classes([IsAuthenticated])
def deleteCategory(request, pk):

    try:
        # res = Category.objects.filter(_id=pk).delete()
        entry = Category.objects.get(_id=pk)
        entry.delete()
        return Response({"_id": pk, "success": True})
    except Exception as e:
        print(e)
        message = {
            'detail': 'Category not exists'
        }
        return Response(message, status=status.HTTP_400_BAD_REQUEST)


@api_view(['PATCH'])
@permission_classes([IsAuthenticated])
def updateCategory(request, pk):
    data = request.data
    try:
        # res = Category.objects.filter(_id=pk).delete()
        entry = Category.objects.get(_id=pk)
        if "name" in data:
            entry.name = data["name"]
        if "description" in data:
            entry.description = data["description"]
        if "activePlayList" in data:
            entry.activePlayList = data["activePlayList"]
        entry.save()

        serializer = CategorySerializer(entry, many=False)
        return Response(serializer.data)
    except Exception as e:
        print(e)
        message = {
            'detail': 'Category not exists'
        }
        return Response(message, status=status.HTTP_400_BAD_REQUEST)
