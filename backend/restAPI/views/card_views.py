from django.contrib.auth.models import User
from rest_framework.response import Response
from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import IsAuthenticated, IsAdminUser
from restAPI.models import Card, Category
from restAPI.serializers import CardSerializer
from rest_framework import status


@api_view(['GET'])
@permission_classes([IsAuthenticated])
def userCategoryCards(request, category):
    user = request.user
    cards = Card.objects.filter(user=user, category=category)
    serializer = CardSerializer(cards, many=True)
    return Response(serializer.data)


@api_view(['POST'])
@permission_classes([IsAuthenticated])
def createCard(request):
    data = request.data
    user = request.user
    category = Category.objects.get(_id=data['category'])
    try:
        card = Card.objects.create(
            question=data['question'],
            answer=data['answer'],
            user=user,
            category=category,
            activePlayList=data['activePlayList']
        )

        serializer = CardSerializer(card, many=False)
        return Response(serializer.data)
    except Exception as e:
        message = {
            'detail': 'err'
        }
        return Response(message, status=status.HTTP_400_BAD_REQUEST)


@api_view(['DELETE'])
@permission_classes([IsAuthenticated])
def deleteCard(request, pk):

    try:
        # res = Category.objects.filter(_id=pk).delete()
        entry = Card.objects.get(_id=pk)
        entry.delete()
        return Response({"_id": pk, "success": True})
    except Exception as e:
        print(e)
        message = {
            'detail': 'Card not exists'
        }
        return Response(message, status=status.HTTP_400_BAD_REQUEST)


@api_view(['PATCH'])
@permission_classes([IsAuthenticated])
def updateCard(request, pk):
    data = request.data
    print(data)
    try:
        # res = Category.objects.filter(_id=pk).delete()
        entry = Card.objects.get(_id=pk)
        if "question" in data:
            entry.question = data["question"]
        if "answer" in data:
            entry.answer = data["answer"]
        if "activePlayList" in data:
            entry.activePlayList = data["activePlayList"]
            
         
        entry.save()

        serializer = CardSerializer(entry, many=False)
        return Response(serializer.data)
    except Exception as e:
        print(e)
        message = {
            'detail': 'Card not exists'
        }
        return Response(message, status=status.HTTP_400_BAD_REQUEST)
