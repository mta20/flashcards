
from django.contrib import admin
from django.urls import path, include
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
       
    path('admin/', admin.site.urls),
    path('api/users/', include('restAPI.urls.user_urls')),
    path('api/categories/', include('restAPI.urls.category_urls')),
    path('api/cards/', include('restAPI.urls.card_urls')),
    path("", include('base.urls')),
]
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

