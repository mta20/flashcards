from django.shortcuts import render
from django.template.loader import render_to_string
from django.http import HttpResponse
# Create your views here.
def landing(request):
    response_data=render_to_string('landing/landing.html')
    return HttpResponse(response_data)

def main(request):
    # response_data=render_to_string('index.html')
    return render(request,'index.html')