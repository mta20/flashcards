from django.urls import path
from base import views
from django.conf.urls import url

urlpatterns = [
    path('', views.landing,
         name="landing"),
    url(r'^(?:.*)/?$', views.main,
         name="landing"), 
]
