import { useEffect } from 'react'
import { useSelector } from "react-redux";
import { useHistory } from "react-router-dom";
// import {  withRouter} from 'react-router-dom';
function CheckAuth() {
    let history = useHistory();
    const { loadingUserInfo, userInfo } = useSelector(state => state.auth)



    useEffect(() => {

        if (!loadingUserInfo) {
            if (!userInfo) {
                window.location.href = '/login';
            }


        }
    }, [loadingUserInfo, userInfo, history])

}

export default CheckAuth
