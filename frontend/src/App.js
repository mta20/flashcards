
import {
  BrowserRouter as Router,
  Switch,
  Route
} from "react-router-dom";
import Login from "./screens/Login";
import Dashboard from "./screens/Dashboard";
import CategoryCards from "./screens/CategoryCards";
import "./assets/styles/global.scss"
import UserProfile from "./screens/UserProfile";

const App = () => {

  return (
    <Router>
      <Switch>

        <Route path="/login" component={Login} />
        <Route exact path="/dashboard" component={Dashboard} />
        <Route path="/dashboard/cards/:id" component={CategoryCards} />
        <Route path="/dashboard/user-profile" component={UserProfile} />
      </Switch>
    </Router>
  );
}
export default App