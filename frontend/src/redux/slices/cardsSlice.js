import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import axios from "axios";

export const getCardsAction = createAsyncThunk(
  "cards/getAll",
  async (id, { getState }) => {
    try {
      const config = {
        headers: {
          "Content-Type": "application/json",
          Authorization: `Bearer ${getState().auth.userInfo.token}`,
        },
      };
      const { data } = await axios.get(
        `/api/cards/category/${id}/`,

        config
      );
      return data;
    } catch (err) {
      throw Error(
        err.response && err.response.data.detail
          ? err.response.data.detail
          : err.message
      );
    }
  }
);

export const deleteCardAction = createAsyncThunk(
  "cards/delete",
  async (dataId, { getState, dispatch }) => {
    try {
      const config = {
        headers: {
          "Content-Type": "application/json",
          Authorization: `Bearer ${getState().auth.userInfo.token}`,
        },
      };
      const { data } = await axios.delete(
        `/api/cards/delete/${dataId}/`,

        config
      );

      return data;
    } catch (err) {
      throw Error(
        err.response && err.response.data.detail
          ? err.response.data.detail
          : err.message
      );
    }
  }
);

export const createCardAction = createAsyncThunk(
  "card/create",
  async (dataCreate, { getState, dispatch }) => {

    const { question, answer, category, activePlayList } = dataCreate;
    try {
      const config = {
        headers: {
          "Content-Type": "application/json",
          Authorization: `Bearer ${getState().auth.userInfo.token}`,
        },
      };
      const { data } = await axios.post(
        "/api/cards/create/",
        {
          question,
          answer,
          category,
          activePlayList
        },
        config
      );

      return data;
    } catch (err) {
      throw Error(
        err.response && err.response.data.detail
          ? err.response.data.detail
          : err.message
      );
    }
  }
);

export const updateCardAction = createAsyncThunk(
  "cards/update",
  async (dataUpdate, { getState, dispatch }) => {
    const { answer, question, _id, activePlayList } = dataUpdate;

    try {
      const config = {
        headers: {
          "Content-Type": "application/json",
          Authorization: `Bearer ${getState().auth.userInfo.token}`,
        },
      };
      const { data } = await axios.patch(
        `/api/cards/update/${_id}/`,
        {
          answer, question, activePlayList
        },
        config
      );

      return data;
    } catch (err) {
      throw Error(
        err.response && err.response.data.detail
          ? err.response.data.detail
          : err.message
      );
    }
  }
);

export const cardsSlice = createSlice({
  name: "cards",
  initialState: { cards: [] },
  reducers: {
    actionReset(state) {
      return { ...state, action: null }
    }
  },
  extraReducers: (builder) => {
    builder
      .addCase(getCardsAction.pending, (state, action) => {
        return { loading: true, action: 'get' };
      })
      .addCase(getCardsAction.fulfilled, (state, action) => {
        return { loading: false, cards: action.payload };
      })
      .addCase(getCardsAction.rejected, (state, action) => {
        return { loading: false, error: action.error.message };
      }) // deleting
      .addCase(deleteCardAction.pending, (state, action) => {
        state.loading = true;
        state.action = 'delete';
      })
      .addCase(deleteCardAction.fulfilled, (state, action) => {
        state.loading = false;
        state.cards = state.cards.filter((card) => {
          return parseInt(card["_id"]) !== parseInt(action.payload["_id"]);
        });
      })
      .addCase(deleteCardAction.rejected, (state, action) => {
        state.loading = false;
        state.error = action.error.message;
      }) //create
      .addCase(createCardAction.pending, (state, action) => {
        state.loading = true;
        state.action = "create";
      })
      .addCase(createCardAction.fulfilled, (state, action) => {
        state.loading = false;
        state.cards.push(action.payload);
      })
      .addCase(createCardAction.rejected, (state, action) => {
        state.loading = false;
        state.error = action.error.message;
      }) // update
      .addCase(updateCardAction.pending, (state, action) => {
        state.loading = true;
        state.action = 'update';
      })
      .addCase(updateCardAction.fulfilled, (state, action) => {
        state.loading = false;
        state.cards = state.cards.map((card) => {

          if (parseInt(card["_id"]) === parseInt(action.payload["_id"])) {
            card["answer"] = action.payload["answer"]
            card["question"] = action.payload["question"]
            card["activePlayList"] = action.payload["activePlayList"]
            return card
          }
          else {
            return card
          }
        })
      })
      .addCase(updateCardAction.rejected, (state, action) => {
        state.loading = false;
        state.error = action.error.message;
      });
  },
});
export const { actionReset } = cardsSlice.actions;
export default cardsSlice.reducer;
