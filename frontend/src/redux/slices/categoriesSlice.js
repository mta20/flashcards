import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import axios from "axios";


export const getCategoriesAction = createAsyncThunk(
  "categories/getAll",
  async (data, { getState }) => {
    try {
      const config = {
        headers: {
          "Content-Type": "application/json",
          "Authorization": `Bearer ${getState().auth.userInfo.token}`
        },
      };
      const { data } = await axios.get(
        "/api/categories/",

        config
      );
      return data;
    } catch (err) {
      throw Error(
        err.response && err.response.data.detail
          ? err.response.data.detail
          : err.message
      );
    }
  }
);

export const deleteCategoryAction = createAsyncThunk(
  "categories/delete",
  async (dataId, { getState, dispatch }) => {
    try {
      const config = {
        headers: {
          "Content-Type": "application/json",
          Authorization: `Bearer ${getState().auth.userInfo.token}`,
        },
      };
      const { data } = await axios.delete(
        `/api/categories/delete/${dataId}/`,

        config
      );
      return data;
    } catch (err) {
      throw Error(
        err.response && err.response.data.detail
          ? err.response.data.detail
          : err.message
      );
    }
  }
);


export const createCategoryAction = createAsyncThunk(
  "categories/create",
  async (dataCreate, { getState, dispatch }) => {

    const { name, description } = dataCreate
    try {
      const config = {
        headers: {
          "Content-Type": "application/json",
          Authorization: `Bearer ${getState().auth.userInfo.token}`,
        },
      };
      const { data } = await axios.post(
        "/api/categories/create/",
        {
          name, description
        },
        config
      );


      return data;
    } catch (err) {
      throw Error(
        err.response && err.response.data.detail
          ? err.response.data.detail
          : err.message
      );
    }
  }
);

export const updateCategoryAction = createAsyncThunk(
  "categories/update",
  async (dataUpdate, { getState, dispatch }) => {
    const { name, _id } = dataUpdate
    try {
      const config = {
        headers: {
          "Content-Type": "application/json",
          Authorization: `Bearer ${getState().auth.userInfo.token}`,
        },
      };
      const { data } = await axios.patch(
        `/api/categories/update/${_id}/`,
        {
          name
        },
        config
      );
      return data;
    } catch (err) {
      throw Error(
        err.response && err.response.data.detail
          ? err.response.data.detail
          : err.message
      );
    }
  }
);

export const categoriesSlice = createSlice({
  name: "categories",
  initialState: { categories: [] },
  reducers: {
    actionReset(state) {
      return { ...state, action: null }
    }
  },
  extraReducers: (builder) => {
    builder
      .addCase(getCategoriesAction.pending, (state, action) => {
        return { loading: true, action: 'get' };
      })
      .addCase(getCategoriesAction.fulfilled, (state, action) => {
        return { loading: false, categories: action.payload };
      })
      .addCase(getCategoriesAction.rejected, (state, action) => {
        return { loading: false, error: action.error.message };
      }) // deleting
      .addCase(deleteCategoryAction.pending, (state, action) => {
        state.loading = true;
        state.action = 'delete';
      })
      .addCase(deleteCategoryAction.fulfilled, (state, action) => {
        state.loading = false;
        state.categories = state.categories.filter(category => parseInt(category._id) !== parseInt(action.payload._id))
      })
      .addCase(deleteCategoryAction.rejected, (state, action) => {
        state.loading = false;
        state.error = action.error.message;
      }) //create
      .addCase(createCategoryAction.pending, (state, action) => {
        state.loading = true;
        state.action = "create";
      })
      .addCase(createCategoryAction.fulfilled, (state, action) => {
        state.loading = false;
        state.categories.push(action.payload);
      })
      .addCase(createCategoryAction.rejected, (state, action) => {
        state.loading = false;
        state.error = action.error.message;
      }) // update
      .addCase(updateCategoryAction.pending, (state, action) => {
        state.loading = true;
        state.action = 'update';
      })
      .addCase(updateCategoryAction.fulfilled, (state, action) => {
        state.loading = false;
        state.categories = state.categories.map((category) => {
          return parseInt(category["_id"]) === parseInt(action.payload["_id"]) ?
            { ...category, name: action.payload["name"] } : category
        })

      })
      .addCase(updateCategoryAction.rejected, (state, action) => {
        state.loading = false;
        state.error = action.error.message;
      });
  },
});

export const { actionReset } = categoriesSlice.actions;
export default categoriesSlice.reducer;

