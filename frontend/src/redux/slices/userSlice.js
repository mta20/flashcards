import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import axios from "axios";

//method - external action for requesting
export const loginAction = createAsyncThunk("user/login",
  async (data) => {
    const { username, password } = data
    try {
      const config = {
        headers: {
          "Content-Type": "application/json",

        },
      };
      const { data } = await axios.post(
        "/api/users/login/",
        {
          username, password
        },
        config
      );
      localStorage.setItem("userInfo", JSON.stringify(data))
      return data
    } catch (err) {
      throw Error(err.response && err.response.data.detail ? err.response.data.detail : err.message) // erro of app is err message
    }
  }
);

export const registerAction = createAsyncThunk("user/register",
  async (data) => {
    const { name, family, email, password } = data
    try {
      const config = {
        headers: {
          "Content-Type": "application/json",
        },
      };
      const { data } = await axios.post(
        "/api/users/register/",
        {
          name, family, email, password
        },
        config
      );
      localStorage.setItem("userInfo", JSON.stringify(data))
      return data
    } catch (err) {
      throw Error(err.response && err.response.data.detail ? err.response.data.detail : err.message) // erro of app is err message
    }
  }
);

export const updateProfileAction = createAsyncThunk("user/updateProfile",
  async (data, { getState }) => {
    const { name, family, email, password } = data

    try {
      const config = {
        headers: {
          "Content-Type": "application/json",
          "Authorization": `Bearer ${getState().auth.userInfo.token}`
        },
      };

      const { data } = await axios.put("/api/users/profile/update/",
        {
          name, family, email, password
        },
        config
      );
      localStorage.setItem("userInfo", JSON.stringify(data))
      return data
    } catch (err) {
      throw Error(err.response && err.response.data.detail ? err.response.data.detail : err.message) // erro of app is err message
    }
  }
);


const userInfoFromStorage = localStorage.getItem("userInfo") ?
  JSON.parse(localStorage.getItem("userInfo")) : null

export const userSlice = createSlice({
  name: "user",
  initialState: { userInfo: userInfoFromStorage },
  reducers: {
    logout: (state, action) => {
      return {}
    },
    actionReset(state) {
      return { ...state, action: null }
    }
  },
  extraReducers: builder => {
    builder
      .addCase(loginAction.pending, (state, action) => {
        return { loading: true }
      })
      .addCase(loginAction.fulfilled, (state, action) => {
        return { loading: false, userInfo: action.payload }
      })
      .addCase(loginAction.rejected, (state, action) => {
        return { loading: false, error: action.error.message }
      })//register
      .addCase(registerAction.pending, (state, action) => {
        return { loading: true }
      })
      .addCase(registerAction.fulfilled, (state, action) => {
        return { loading: false, userInfo: action.payload }
      })
      .addCase(registerAction.rejected, (state, action) => {
        return { loading: false, error: action.error.message }
      })//update profile
      .addCase(updateProfileAction.pending, (state, action) => {
        state.loading = true
        state.action = "update"
      })
      .addCase(updateProfileAction.fulfilled, (state, action) => {
        state.loading = false
        state.userInfo = action.payload

      })
      .addCase(updateProfileAction.rejected, (state, action) => {
        state.loading = false;
        state.error = action.error.message;
        state.userInfo = {}

      })

  }
});

export const { logout, actionReset } = userSlice.actions;
export default userSlice.reducer;


