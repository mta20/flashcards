import { configureStore } from "@reduxjs/toolkit"; //naming import
import useReducer from "./slices/userSlice";
import categoryReducer from "./slices/categoriesSlice";
import cardsReducer from "./slices/cardsSlice";


const store = configureStore({
  reducer: {
    auth: useReducer,

    categories: categoryReducer,

    cards: cardsReducer
  },
});
export default store;
