import {
    Container,
    Grid,
    Col,
    Input,
    PasswordInput,
    InputWrapper,
    Button,
    Group,
    Drawer,
    Loader,
    Alert,
    Anchor
} from '@mantine/core'
import { useDispatch, useSelector } from "react-redux";
import { useForm } from '@mantine/hooks';

import React, { useState, useEffect } from 'react';
import { AiOutlineLock, AiOutlineMail, AiOutlineUserAdd, AiOutlineLogin, AiOutlineCloseCircle } from "react-icons/ai";
import Register from '../components/Register';
import { loginAction, registerAction } from "../redux/slices/userSlice";
import { Link } from 'react-router-dom';

const Login = ({ history }) => {
    const [opened, setOpened] = useState(false);
    const [registerReq, setRegisterReq] = useState(false);
    const { error, loading, userInfo } = useSelector(state => state.auth)

    const dispatch = useDispatch();
    useEffect(() => {
        if (userInfo) {
            history.push("/dashboard")
        }
    }, [userInfo, history])


    const loginForm = useForm({
        initialValues: {
            email: '',
            password: '',
        },

        validationRules: {
            email: (value) => /^\S+@\S+$/.test(value),
            password: (value) => /^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{6,}$/.test(value),
        },
    });

    const handleLogin = () => {
        const { email, password } = loginForm.values;
        dispatch(loginAction({ username: email, password }));
    };

    const handleRegister = (data) => {

        dispatch(registerAction(data));
        setRegisterReq(true)
    };

    return (
        <Container style={{ height: "100%" }}>
            <Drawer
                opened={opened}
                onClose={() => {
                    setOpened(false)
                    setRegisterReq(false)
                }}
                padding="xl"
                size="xl"
                title="Register"
                transition="rotate-left"
                transitionDuration={250}
                transitionTimingFunction="ease"
            >
                <Register onSubmit={handleRegister} noShadow noPadding />
                {error && registerReq && (
                    <Alert mt="lg" icon={<AiOutlineCloseCircle size={16} />} title="Failure!" color="red">
                        {error}
                    </Alert>

                )}
            </Drawer>

            <Grid justify="center" align="center" style={{ height: "100%" }}>
                <Col span={12} md={6} lg={4} >


                    <form onSubmit={loginForm.onSubmit(handleLogin)}>
                        <InputWrapper
                            id="username"
                            required
                            label="Email"
                            size="md"
                            mb="sm"

                            error={loginForm.errors.email && 'Field should contain a valid email'}
                        >
                            <Input
                                icon={<AiOutlineMail />}
                                size="lg"
                                required
                                type="email"
                                placeholder="Enter your email"
                                value={loginForm.values.email}
                                onChange={(event) => loginForm.setFieldValue('email', event.currentTarget.value)}
                                onFocus={() => loginForm.setFieldError('email', false)}

                            />
                        </InputWrapper>
                        <InputWrapper
                            id="password"
                            size="md"
                            required
                            label="Password"
                            mb="md"
                            error={
                                loginForm.errors.password &&
                                'Password should contain 1 number, 1 letter and at least 6 characters'
                            }
                        >
                            <PasswordInput
                                size="lg"
                                icon={<AiOutlineLock />}
                                placeholder="Enter your password"
                                value={loginForm.values.password}
                                onChange={(event) => loginForm.setFieldValue('password', event.currentTarget.value)}
                                onFocus={() => loginForm.setFieldError('password', false)}

                            />
                        </InputWrapper>
                        <Group position="apart">
                            <Button
                                size="md"
                                variant="gradient"
                                gradient={{ from: 'grape', to: 'pink', deg: 35 }}
                                leftIcon={<AiOutlineLogin />}
                                type="submit"
                            >
                                {loading ? <Loader size="xs" color="white" variant="dots" /> : "Login"}
                            </Button>

                            <Button
                                size="md"
                                variant="outline"
                                color="grape"
                                leftIcon={<AiOutlineUserAdd />}
                                onClick={() => setOpened(true)}
                            >
                                Register
                            </Button>
                        </Group>
                        <Anchor size="sm" sx={{ display: 'block' }} mt="lg" href="/">
                            Back to home page
                        </Anchor>
                    </form>
                    {error && !registerReq && (
                        <Alert mt="lg" icon={<AiOutlineCloseCircle size={16} />} title="Failure!" color="red">
                            The username or password is incorrect!
                        </Alert>

                    )}
                </Col>
            </Grid>

        </Container >
    )
}
export default Login