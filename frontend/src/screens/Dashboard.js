import React from 'react';
import { AppShell, Header, Container } from '@mantine/core';

import Categories from '../components/Category/Categories';
import MainHeader from '../components/Layout/MainHeader';

function Dashboard() {

  return (
    <AppShell
      navbarOffsetBreakpoint="sm"
      fixed

      header={
        <Header height={70} padding="md">
          <MainHeader withoutNav />
        </Header>
      }
    >
      <Container size="xs" style={{ paddingLeft: 0 }} >
        <Categories />
      </Container >
    </AppShell >
  );
}
export default Dashboard;
