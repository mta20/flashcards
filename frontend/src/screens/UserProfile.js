import React, { useEffect } from 'react'
import Register from '../components/Register'
import { AppShell, Header, Container } from '@mantine/core';
import MainHeader from '../components/Layout/MainHeader';
import { useDispatch, useSelector } from 'react-redux';
import { updateProfileAction, actionReset } from '../redux/slices/userSlice'
import { useNotifications } from '@mantine/notifications';
import { AiOutlineCheck } from "react-icons/ai";
function UserProfile() {
    const { loading, userInfo, error, action } = useSelector(state => state.auth)
    const dispatch = useDispatch()
    const notifications = useNotifications();
    const handleUpdate = ({ name, family, email, password }) => {
        dispatch(updateProfileAction({ name, family, email, password }))
    }
    useEffect(() => {
        if (!loading && !error && action) {
            notifications.showNotification({
                color: 'green',
                title: 'Success',
                message: 'Profile Updated.',
                icon: <AiOutlineCheck />
            })
            dispatch(actionReset())
        }
    }, [loading, userInfo, error])// eslint-disable-line react-hooks/exhaustive-deps
    return (
        <>

            <AppShell
                navbarOffsetBreakpoint="sm"
                fixed

                header={
                    <Header height={70} padding="md">
                        <MainHeader withoutNav />
                    </Header>
                }
            >
                <Container size="xs" style={{ paddingLeft: 0 }}  >
                    <Register formType="update" onSubmit={handleUpdate} />
                </Container >
            </AppShell >

        </>
    )
}

export default UserProfile
