import React, { useEffect, useState } from 'react';
import {
  AppShell,
  Navbar,
  Text,
  Breadcrumbs,
  Paper,
  Header,
  Group,
  ActionIcon
} from '@mantine/core';
import { GiRapidshareArrow } from "react-icons/gi";

import { Link } from 'react-router-dom';
import SanitizedHTML from 'react-sanitized-html';


import ReactCardFlip from 'react-card-flip';
import Cards from '../components/Card/Cards';
import MainHeader from '../components/Layout/MainHeader';
import { useSelector } from "react-redux";
import {
  GrNext,
  GrPrevious
} from "react-icons/gr";

function CategoryCards() {
  const { cards } = useSelector(state => state.cards)

  const [opened, setOpened] = useState(false);
  const [playStatus, setPlayStatus] = useState(false);
  const [playList, setPlayList] = useState([]);
  const [selectedCard, setSelectedCard] = useState()
  const [selectedPalyListIndex, setSelectedPalyListIndex] = useState(0)
  const [isFlipped, setIsFlipped] = useState(false)
  const handleClick = (e) => {
    e.preventDefault();
    setIsFlipped(prevState => !prevState);
  }

  useEffect(() => {
    if (cards) {
      const data = cards.filter((card) => {
        return card.activePlayList === true
      })
      setPlayList(data)
    }

  }, [cards])

  useEffect(() => {

    if (playList) {

      playStatus && setSelectedCard(playList[0])
      !playStatus && setSelectedCard({ _id: -1 })
    }

  }, [playStatus])// eslint-disable-line react-hooks/exhaustive-deps

  useEffect(() => {
    setIsFlipped(false)
    window.hljs.highlightAll();
  }, [selectedCard])

  const handleNextCard = () => {
    if (playList) {
      if (selectedPalyListIndex < playList.length - 1) {
        setSelectedCard(playList[parseInt(selectedPalyListIndex) + 1])
        setSelectedPalyListIndex(selectedPalyListIndex + 1)
      }
      else {
        setSelectedCard(playList[0])
        setSelectedPalyListIndex(0)
      }
    }
  }
  const handlePreviousCard = () => {
    if (playList) {
      if (selectedPalyListIndex > 0) {
        setSelectedCard(playList[parseInt(selectedPalyListIndex) - 1])
        setSelectedPalyListIndex(selectedPalyListIndex - 1)
      }
      else {
        setSelectedCard(playList[playList.length - 1])
        setSelectedPalyListIndex(playList.length - 1)
      }
    }
  }
  return (

    < AppShell
      navbarOffsetBreakpoint="sm"
      fixed
      navbar={

        < Navbar
          hiddenBreakpoint="sm"
          hidden={!opened
          }
          width={{ sm: 350, lg: 400 }}
          padding="xs" >

          <Navbar.Section mt="xs" >
            <Paper padding="0 15px">
              <Breadcrumbs className="breadcrumbs" >
                <Link to='/dashboard'>Categories</Link>
                <Text size="sm">Cards</Text>
              </Breadcrumbs>
            </Paper>

          </Navbar.Section>

          <Navbar.Section grow mt="lg">


            <Cards onSelect={(data, index = 0) => {
              setSelectedCard(data)
              setSelectedPalyListIndex(index)
            }} playStatus={playStatus} onPlay={setPlayStatus} playList={playList} />

          </Navbar.Section>

          {/* <Navbar.Section>User</Navbar.Section> */}
        </Navbar >
      }
      header={
        < Header height={70} padding="md" >
          <MainHeader handleOpen={() => setOpened((o) => !o)} opened={opened} />
        </Header >
      }
    >

      {(!selectedCard || parseInt(selectedCard['_id']) === -1) ?
        <Text>There is no any selected card ...</Text> :
        playStatus ?
          <>
            <Group grow position="center" direction="column">
              <ReactCardFlip isFlipped={isFlipped} flipDirection="horizontal">
                <div className="box-card">
                  <ActionIcon className="rotate-button" size="lg" onClick={handleClick}><GiRapidshareArrow size="20px" /></ActionIcon>

                  <SanitizedHTML html={selectedCard.question} />
                </div>

                <div className="box-card">
                  <ActionIcon className="rotate-button" size="lg" onClick={handleClick}><GiRapidshareArrow size="20px" /></ActionIcon>

                  <SanitizedHTML html={selectedCard.answer} />
                </div>

              </ReactCardFlip>

            </Group>

            <Group mt="sm" position="center">
              <ActionIcon variant="outline" onClick={handlePreviousCard}>
                <GrPrevious />
              </ActionIcon>
              <ActionIcon variant="outline" onClick={handleNextCard}>
                <GrNext />
              </ActionIcon>
            </Group>

          </> :
          <>
            <Group grow position="center" direction="column">
              <ReactCardFlip isFlipped={isFlipped} flipDirection="horizontal">
                <div className="box-card">
                  <ActionIcon className="rotate-button" size="lg" onClick={handleClick}><GiRapidshareArrow size="20px" /></ActionIcon>

                  <SanitizedHTML html={selectedCard.question} />
                </div>

                <div className="box-card">
                  <ActionIcon className="rotate-button" size="lg" onClick={handleClick}><GiRapidshareArrow size="20px" /></ActionIcon>

                  <SanitizedHTML html={selectedCard.answer} />
                </div>

              </ReactCardFlip>

            </Group>


          </>
      }

    </AppShell >
  );
}
export default CategoryCards;
