import React, { useEffect, useState } from 'react'
import {
    getCardsAction,
    createCardAction,
    deleteCardAction,
    updateCardAction,
    actionReset
} from "../../redux/slices/cardsSlice";
import { useSelector, useDispatch } from "react-redux";
import CardItem from './CardItem';
import { useParams } from 'react-router';
import { Paper, Group, Drawer, LoadingOverlay, Space, Button, ActionIcon, ThemeIcon, Tabs } from '@mantine/core';

import { Scrollbars } from 'react-custom-scrollbars';
import { AiOutlinePlus, AiOutlinePlayCircle, AiFillSave, AiOutlineClose, AiOutlineCheck } from "react-icons/ai";

import { BsStopCircle } from "react-icons/bs";
import { useNotifications } from '@mantine/notifications';
import RichEditor from '../RichEditor/RichEditor';

const Cards = ({ onSelect, onPlay, playStatus, playList }) => {
    const { loading, cards, action, error } = useSelector(state => state.cards)
    const dispatch = useDispatch()
    const { id } = useParams()
    const [showCardForm, setShowCardForm] = useState(false);
    const [cardFormType, setCardFormType] = useState() // update - create
    const [currentCardID, setCurrentCardID] = useState(-1)
    const [question, setQuestion] = useState("");
    const [answer, setAnswer] = useState("");
    const notifications = useNotifications();

    useEffect(() => {
        dispatch(getCardsAction(id));

    }, [id]);// eslint-disable-line react-hooks/exhaustive-deps


    const handlerAddCard = (e) => {
        e.preventDefault()
        const initCard = {
            question: question,
            answer: answer,
            category: id,
            activePlayList: false
        };

        dispatch(createCardAction(initCard))


    };
    const handlerShowCardForm = (formType, data = {}) => {
        setShowCardForm(true)

        if (formType === 'update') {
            setCardFormType('update')
            setQuestion(data["question"])
            setAnswer(data["answer"])
            setCurrentCardID(data["_id"])
        }
        else if (formType === 'create') {
            setCardFormType('create')
            setQuestion("")
            setAnswer("")
        }

    }
    const handlerDeleteCard = (id) => {

        dispatch(deleteCardAction(id));

        setCurrentCardID(-1);

    }
    const handlerUpdateCard = (e) => {
        e.preventDefault()
        dispatch(updateCardAction({
            answer: answer,
            question: question,
            _id: currentCardID
        }));

    };
    const handleEditCustomFields = (data) => {
        dispatch(updateCardAction({
            ...data,
        }));
    }
    useEffect(() => {

        if (!loading) {
            let messages = {}
            if (action) {
                if (error) {
                    messages = {
                        create: 'Some thing is wrong!',
                        update: 'Some thing is wrong!',
                        delete: 'Some thing is wrong! Refresh page and try again.'
                    }
                }
                else {
                    messages = {
                        create: 'Card Added!',
                        update: 'Card Updated!',
                        delete: 'Card deleted!'
                    }
                }

                notifications.showNotification({
                    color: error ? 'red' : 'green',
                    title: error ? 'Failure' : 'Success',
                    message: messages[action],
                    icon: error ? <AiOutlineClose /> : <AiOutlineCheck />
                })
                dispatch(actionReset())
                setShowCardForm(false)
                onSelect({ _id: currentCardID, answer, question })
            }
        }
    }, [cards, loading, action, error])// eslint-disable-line react-hooks/exhaustive-deps

    return (
        <>

            <Drawer
                opened={showCardForm}

                onClose={() => setShowCardForm(false)}
                title={cardFormType === 'update' ? "Edit card" : "Add Card"}
                size="lg"
                padding="20px 8px 40px"
                shadow="xs"
            >
                <Scrollbars>
                    <form onSubmit={cardFormType === 'update' ? handlerUpdateCard : handlerAddCard}>
                        <LoadingOverlay loaderProps={{ size: 'sm', color: 'grape', variant: 'dots' }} visible={loading} />

                        <RichEditor
                            value={question}
                            html={(data) => setQuestion(data)}
                            placeholder="Write question here..." />
                        <Space />
                        <RichEditor
                            value={answer}
                            html={(data) => setAnswer(data)}
                            placeholder="Write answer here..." />


                        <Button
                            size="sm"
                            mt="md"
                            mb="xl"
                            variant="gradient"
                            gradient={{ from: 'grape', to: 'pink', deg: 35 }}
                            leftIcon={<AiFillSave />}
                            type="submit"
                        >
                            Save
                        </Button>
                    </form>
                </Scrollbars>
            </Drawer>

            <Paper padding="0 15px" mb="md">
                <Group spacing="5px">
                    <ActionIcon size="lg" onClick={() => handlerShowCardForm('create')}><ThemeIcon radius="lg" variant="gradient" gradient={{ from: 'grape', to: 'pink', deg: 35 }}><AiOutlinePlus size="20px" /></ThemeIcon></ActionIcon>
                    <ActionIcon size="lg" onClick={() => {
                        onPlay(!playStatus)
                        onSelect({ _id: -1 })
                    }}><ThemeIcon radius="lg" variant="gradient" gradient={{ from: 'teal', to: 'lime', deg: 105 }}>{!playStatus ? <AiOutlinePlayCircle size="20px" /> : <BsStopCircle size="20px" />}</ThemeIcon></ActionIcon>
                </Group>
            </Paper>
            <Scrollbars>
                <LoadingOverlay loaderProps={{ size: 'sm', color: 'grape', variant: 'dots' }} visible={loading} />

                <Tabs mb="70px" active={playStatus ? 1 : 0}>

                    <Tabs.Tab label="Cards" disabled={playStatus}>

                        {cards && cards.map(({ _id, answer, question, activePlayList }) => {
                            return (
                                <div key={_id} onClick={() => onSelect({ _id, answer, question, activePlayList })} >
                                    <CardItem question={question} answer={answer} isInPlayList={activePlayList} id={_id} onDelete={handlerDeleteCard} onEditCustomFields={(data) => handleEditCustomFields(data)} onEdit={() => handlerShowCardForm('update', { activePlayList, question, answer, _id })} />
                                </div>
                            )
                        })}
                    </Tabs.Tab>



                    <Tabs.Tab label="Play List" disabled={!playStatus}>

                        {playList && playList.map(({ _id, answer, question, activePlayList }, index) => {

                            return activePlayList === true && (
                                <div key={_id} onClick={() => onSelect({ _id, answer, question, activePlayList }, index)} >
                                    <CardItem question={question} answer={answer} isInPlayList={activePlayList} id={_id} onDelete={handlerDeleteCard} onEditCustomFields={(data) => handleEditCustomFields(data)} onEdit={() => handlerShowCardForm('update', { activePlayList, question, answer, _id })} />
                                </div>
                            )

                        })}
                    </Tabs.Tab>



                </Tabs>

            </Scrollbars>
        </>
    )
}

export default Cards
