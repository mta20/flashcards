import React, { useState } from 'react'
import { Paper, Menu, Group, Text, Tooltip, Switch, Skeleton } from '@mantine/core';
import { AiOutlineEdit, AiOutlineDelete } from "react-icons/ai";
import SanitizedHTML from 'react-sanitized-html';
import { useModals } from '@mantine/modals'

function CardItem(props) {
    const [opened, setOpened] = useState(false);
    const modals = useModals();
    let loading = false

    const openDeleteModal = () => {

        modals.openConfirmModal({
            title: 'Delete Card',
            children: (
                <Text size="sm">
                    Are you sure you want to delete this card?
                </Text>
            ),
            labels: { confirm: 'Delete card', cancel: "No don't delete it" },
            confirmProps: { color: 'red' },
            onConfirm: () => props.onDelete(props.id),
        });
    }

    const handleActivePlayList = (e) => {

        e.preventDefault()
        props.onEditCustomFields({ _id: props.id, activePlayList: !props.isInPlayList })

    }

    if (loading) {
        return <Paper padding="md" radius={0} shadow="0 1px 1px #e5e5e5 inset">
            <Skeleton height={8} radius="xl" />
            <Skeleton height={8} mt={6} width="70%" radius="xl" />
        </Paper >
    }
    return (
        <Paper
            padding="md"
            radius={0}


            sx={(theme) => ({
                cursor: "pointer",
                height: '70px',
                maxHeight: '70px',
                transition: "all .3s",
                borderBottom: '1px solid #e5e5e5',
                '&:hover': {
                    backgroundColor:
                        theme.colorScheme === 'dark' ? theme.colors.dark[5] : theme.colors.gray[1],
                },
            })}
        >



            <Group position="apart" >

                <Text className="card-question" >
                    <SanitizedHTML html={props.question} />

                </Text>

                <Group spacing={6}>

                    <Tooltip position="top-end" withArrow transitionDuration={300} transition="scale-x" color="grape" label={`${props.isInPlayList ? 'Remove from' : 'Add to'} play list`}  >
                        <Switch
                            onClick={(e) => e.stopPropagation()}
                            radius="xs"
                            checked={props.isInPlayList} color="grape"
                            onChange={handleActivePlayList}
                        />
                    </Tooltip>

                    <Menu onClick={(e) => e.stopPropagation()} withArrow zIndex={1000} position="left" placement="center" opened={opened} onOpen={() => setOpened(true)} onClose={() => setOpened(false)}>
                        <Menu.Item icon={<AiOutlineEdit />} onClick={() => props.onEdit(props.id)} >Edit Card</Menu.Item>,
                        <Menu.Item icon={<AiOutlineDelete />} onClick={openDeleteModal}>Delete Card</Menu.Item>
                    </Menu>
                </Group>
            </Group >
        </Paper >
    )
}

export default CardItem
