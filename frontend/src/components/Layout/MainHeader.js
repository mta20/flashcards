import React from 'react';
import {
    Text,
    MediaQuery,
    Burger,
    Menu,
    Avatar,
    Group
} from '@mantine/core';
import { useDispatch } from "react-redux";
import { Link } from 'react-router-dom';
import CheckAuth from '../../hooks/CheckAuth';
import { logout } from "../../redux/slices/userSlice";
function MainHeader({ handleOpen, opened, withoutNav }) {
    CheckAuth()

    const dispatch = useDispatch();
    const logoutHandler = () => {

        dispatch(logout());
        localStorage.removeItem("userInfo")


    };
    return (


        <div style={{ display: 'flex', alignItems: 'center', height: '100%', justifyContent: 'space-between' }}>
            <Link to="/dashboard" className="logo">
                <img style={{ height: '50px' }} src="/static/logo12.png"></img>
            </Link>


            <Group>
                <Menu trigger="click"
                    zIndex={1000}
                    delay={500}
                    closeOnScroll={false}
                    transition="rotate-left"
                    transitionDuration={100}
                    position="left"
                    withArrow
                    transitionTimingFunction="ease"
                    control={
                        <Avatar style={{ cursor: 'pointer' }} radius="xl" src={null} alt="Click to show menu" color="grape" />
                    }
                >

                    <Menu.Item component={Link} to="/dashboard/user-profile">
                        User Profile
                    </Menu.Item>


                    <Menu.Item
                        onClick={logoutHandler}
                    >
                        Logout
                    </Menu.Item>
                </Menu>

                {!withoutNav &&
                    <MediaQuery smallerThan="sm" styles={{ display: 'none' }}>


                        <Burger
                            opened={opened}
                            onClick={handleOpen}
                            size="sm"
                        />

                    </MediaQuery>
                }

            </Group>

        </div>

    )
}

export default MainHeader
