import { Accordion, createStyles } from '@mantine/core';
import { AiOutlinePlus } from "react-icons/ai";

const useStyles = createStyles((theme, _params, getRef) => {
    const controlRef = getRef('control');
    const iconRef = getRef('icon');

    return {
        icon: { ref: iconRef },

        control: {
            ref: controlRef,
            border: 0,
            opacity: 0.6,
            color: theme.colorScheme === 'dark' ? theme.white : theme.black,

            '&:hover': {
                backgroundColor: '#f8f9fa',
                opacity: 1,
            },
        },

        item: {
            borderBottom: 0,
            overflow: 'hidden',
            transition: `box-shadow 150ms ${theme.transitionTimingFunction}`,
            border: '1px solid transparent',
            borderRadius: theme.radius.sm,
        },

        itemOpened: {
            backgroundColor: theme.colorScheme === 'dark' ? theme.colors.dark[5] : theme.white,
            borderColor: theme.colorScheme === 'dark' ? theme.colors.dark[5] : theme.colors.gray[3],

            [`& .${controlRef}`]: {
                opacity: 1,
            },

            [`& .${iconRef}`]: {
                transform: 'rotate(45deg)',
            },
        },

        content: {
            paddingLeft: 0,
        },
    };
});

function StyledAccordion(props) {
    const { classes } = useStyles();
    return <Accordion classNames={classes} icon={<AiOutlinePlus />} {...props} />;
}
export default StyledAccordion