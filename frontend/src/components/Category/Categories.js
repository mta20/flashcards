import { Group, LoadingOverlay, Paper, Button, Text, Drawer, TextInput } from '@mantine/core'

import React, { useState, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import {
    getCategoriesAction,
    createCategoryAction,
    deleteCategoryAction,
    updateCategoryAction,
    actionReset
} from "../../redux/slices/categoriesSlice";
import CategoryItem from './CategoryItem'
import { useForm } from '@mantine/hooks';
import { useNotifications } from '@mantine/notifications';
import { IoAddOutline, } from "react-icons/io5";
import { AiOutlineClose, AiOutlineCheck } from "react-icons/ai";

function Categories() {
    const [showCategoryForm, setShowCategoryForm] = useState(false)
    const [categoryFormType, setCategoryFormType] = useState() // update - create
    const [categoryIdForUpdate, setCategoryIdForUpdate] = useState(-1)
    const dispatch = useDispatch();
    const {
        loading,
        categories,
        action,
        error
    } = useSelector((state) => state.categories);
    const notifications = useNotifications();



    useEffect(() => {
        dispatch(getCategoriesAction());
    }, []); // eslint-disable-line react-hooks/exhaustive-deps

    const form = useForm({
        initialValues: {
            name: '',
        },

        validationRules: {
            name: (value) => value.trim().length >= 2,
        },
    });


    const handlerAddCategory = () => {
        dispatch(
            createCategoryAction({
                name: form.values.name,
                description: ""
            })
        );
    }
    const handlerUpdateCategory = () => {

        dispatch(
            updateCategoryAction({
                name: form.values.name,
                _id: categoryIdForUpdate
            })

        );
    }
    const handlerDeleteCategory = (id) => {
        dispatch(deleteCategoryAction(id))
    }
    const handlerShowCategoryForm = (formType, data = {}) => {
        setShowCategoryForm(true)

        if (formType === 'update') {
            setCategoryFormType('update')
            form.setFieldValue('name', data.name)
            setCategoryIdForUpdate(data["_id"])
        }
        else if (formType === 'create') {
            setCategoryFormType('create')
            form.setFieldValue('name', '')
        }

    }

    useEffect(() => {
        if (!loading) {
            let messages = {}
            if (action) {
                if (error) {
                    messages = {
                        create: 'Some thing is wrong!',
                        update: 'Some thing is wrong!',
                        delete: 'Some thing is wrong! Refresh page and try again.'
                    }
                }
                else {
                    messages = {
                        create: 'Category Added!',
                        update: 'Category Updated!',
                        delete: 'Category deleted!'
                    }
                }

                notifications.showNotification({
                    color: error ? 'red' : 'green',
                    title: error ? 'Failure' : 'Success',
                    message: messages[action],
                    icon: error ? <AiOutlineClose /> : <AiOutlineCheck />
                })
                dispatch(actionReset())
                form.setFieldValue('name', '')
                setShowCategoryForm(false)
            }
        }
    }, [categories, loading, action, error])// eslint-disable-line react-hooks/exhaustive-deps

    return (
        <>
            <Drawer
                opened={showCategoryForm}

                onClose={() => setShowCategoryForm(false)}
                title="update card"
                size="lg"
                padding="md"
                shadow="xs"
            >
                <form onSubmit={form.onSubmit(categoryFormType === 'update' ? handlerUpdateCategory : handlerAddCategory)}>
                    <LoadingOverlay loaderProps={{ size: 'sm', color: 'grape', variant: 'dots' }} visible={loading} />
                    <TextInput
                        data-autofocus
                        required
                        placeholder="Category name"
                        label="Category name"
                        value={form.values.name}
                        onChange={(event) => form.setFieldValue('name', event.currentTarget.value)}
                        onFocus={() => form.setFieldError('name', false)}
                        error={form.errors.name && 'Category name should include at least 2 characters'}
                        variant='default'
                    />


                    <Button
                        size="sm"
                        mt="md"
                        variant="gradient"
                        gradient={{ from: 'grape', to: 'pink', deg: 35 }}
                        leftIcon={<IoAddOutline />}
                        type="submit"
                    >
                        <span style={{ textTransform: 'capitalize' }}>{categoryFormType}&nbsp;</span> Category
                    </Button>
                </form>
            </Drawer>
            <Button
                size="sm"
                variant="gradient"
                gradient={{ from: 'grape', to: 'pink', deg: 35 }}
                leftIcon={<IoAddOutline size="20px" />}
                mt="xl"
                onClick={() => handlerShowCategoryForm('create')}
            >
                Create Category
            </Button>



            <Group mt="lg" style={{ position: 'relative' }} spacing={0} direction="column">
                <LoadingOverlay loaderProps={{ size: 'sm', color: 'grape', variant: 'dots' }} visible={loading} />

                {categories && categories.length ? categories.map(({ _id, name }) => {
                    return (

                        <CategoryItem key={_id} id={_id} name={name} onDelete={handlerDeleteCategory} onEdit={() => handlerShowCategoryForm('update', { name, _id })} />

                    );
                }) :
                    <Paper
                        shadow="xs"
                        padding="lg"
                        style={{ width: "100%" }}
                    >
                        <Text
                            weight={700}
                            size="md"
                            variant="gradient"
                            gradient={{ from: 'grape', to: 'white', deg: 45 }}
                            style={{ opacity: '.5', userSelect: 'none' }}
                        >Category list is empty! Add a category</Text>
                    </Paper>}

            </Group>

        </>
    )
}

export default Categories
