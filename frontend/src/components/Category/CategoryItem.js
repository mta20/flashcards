import { Group } from '@mantine/core'
import React from 'react'


import { useModals } from '@mantine/modals'
import { AiOutlineEdit, AiOutlineDelete } from "react-icons/ai";
import { Text, ActionIcon, Paper } from '@mantine/core';

import styles from './CategoryItem.module.scss'
import { Link } from 'react-router-dom';
function CategoryItem({ name, id, onDelete, onEdit }) {

    const modals = useModals();

    const openDeleteModal = () => {

        modals.openConfirmModal({
            title: 'Delete Category',
            children: (
                <Text size="sm">
                    Are you sure you want to delete this category?
                </Text>
            ),
            labels: { confirm: 'Delete category', cancel: "No don't delete it" },
            confirmProps: { color: 'red' },
            onConfirm: () => onDelete(id),
        });
    }

    return (
        <Link className={styles.link} to={`/dashboard/cards/${id}`}>
            <Paper radius={0} shadow="sm" withBorder className={styles.category} >
                <Group grow >
                    <Text>{name}</Text>
                    <Group position='right' mr="15px">
                        <ActionIcon onClick={
                            (e) => {
                                e.preventDefault()
                                onEdit()
                            }
                        }>
                            <AiOutlineEdit size="20px" />
                        </ActionIcon>
                        <ActionIcon onClick={
                            (e) => {
                                e.preventDefault()
                                openDeleteModal()
                            }
                        }>
                            <AiOutlineDelete size="20px" />
                        </ActionIcon>
                    </Group>
                </Group>
            </Paper>
        </Link >
    )
}

export default CategoryItem
