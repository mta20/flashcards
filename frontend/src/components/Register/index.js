import React, { useState } from 'react';
import { useForm } from '@mantine/hooks';
import { useSelector } from 'react-redux';
import {
    PasswordInput,
    Button,
    Group,
    Paper,
    TextInput,
    LoadingOverlay,
    Text
} from '@mantine/core'
import { AiOutlineLock, AiOutlineMail, AiOutlineUserAdd, AiOutlineSave } from "react-icons/ai";

const Register = ({ noShadow,
    noPadding,
    noSubmit,
    style, onSubmit, formType = 'register' }) => {

    const { userInfo } = useSelector(state => state.auth)

    const [loading] = useState(false);
    const [error] = useState(null);




    const form = useForm({
        initialValues: formType === 'update' && userInfo ? {
            name: userInfo.name,
            family: userInfo.family,
            email: userInfo.email,
            password: '',
            confirmPassword: '',
        } : {
            name: '',
            family: '',
            email: '',
            password: '',
            confirmPassword: '',
        },

        validationRules: formType === 'update' ? {
            name: (value) => value.trim().length >= 2,
            family: (value) => value.trim().length >= 2,
            email: (value) => /^\S+@\S+$/.test(value),
            password: (value) => value.trim().length === 0 || /^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{6,}$/.test(value),
            confirmPassword: (val, values) => val === values.password,
        } : {
            name: (value) => value.trim().length >= 2,
            family: (value) => value.trim().length >= 2,
            email: (value) => /^\S+@\S+$/.test(value),
            password: (value) => /^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{6,}$/.test(value),
            confirmPassword: (val, values) => val === values.password,
        },
    });

    const handleSubmit = () => {

        onSubmit({ ...form.values })

    };


    return (
        <Paper
            padding={noPadding ? 0 : 'lg'}
            shadow={noShadow ? null : 'sm'}
            style={{
                position: 'relative',
                backgroundColor: '#fff',
                ...style,
            }}
        >
            <form onSubmit={form.onSubmit(handleSubmit)}>
                <LoadingOverlay visible={loading} />

                <div style={{ display: 'flex', marginBottom: 15 }}>
                    <TextInput
                        data-autofocus
                        required
                        placeholder="Your first name"
                        label="First name"
                        style={{ marginRight: 20, flex: '0 0 calc(50% - 10px)' }}
                        value={form.values.name}
                        onChange={(event) => form.setFieldValue('name', event.currentTarget.value)}
                        onFocus={() => form.setFieldError('name', false)}
                        error={form.errors.name && 'First name should include at least 2 characters'}
                        variant='default'
                    />

                    <TextInput
                        required
                        placeholder="Your last name"
                        label="Last name"
                        style={{ flex: '0 0 calc(50% - 10px)' }}
                        value={form.values.family}
                        onChange={(event) => form.setFieldValue('family', event.currentTarget.value)}
                        onFocus={() => form.setFieldError('family', false)}
                        error={form.errors.family && 'Last name should include at least 2 characters'}
                        variant='default'
                    />
                </div>


                <TextInput
                    required
                    placeholder="Your email"
                    label="Email"
                    icon={<AiOutlineMail />}
                    value={form.values.email}
                    onChange={(event) => form.setFieldValue('email', event.currentTarget.value)}
                    onFocus={() => form.setFieldError('email', false)}
                    error={form.errors.email && 'Field should contain a valid email'}

                />

                <PasswordInput
                    style={{ marginTop: 15 }}
                    autoComplete="new-password"
                    placeholder="Password"
                    label="Password"
                    icon={<AiOutlineLock />}
                    value={form.values.password}
                    onChange={(event) => form.setFieldValue('password', event.currentTarget.value)}
                    onFocus={() => form.setFieldError('password', false)}

                    error={
                        form.errors.password &&
                        'Password should contain 1 number, 1 letter and at least 6 characters'
                    }
                />


                <PasswordInput
                    style={{ marginTop: 15 }}
                    autoComplete="new-password"
                    label="Confirm Password"
                    placeholder="Confirm password"
                    icon={<AiOutlineLock />}
                    value={form.values.confirmPassword}
                    onChange={(event) => form.setFieldValue('confirmPassword', event.currentTarget.value)}
                    error={form.errors.confirmPassword && "Passwords don't match. Try again"}
                />



                {error && (
                    <Text color="red" size="sm" style={{ marginTop: 10 }}>
                        {error}
                    </Text>
                )}

                {!noSubmit && (
                    <Group position="apart" style={{ marginTop: 25 }}>


                        <Button
                            size="md"
                            variant="gradient"
                            gradient={{ from: 'grape', to: 'pink', deg: 35 }}
                            leftIcon={formType === 'register' ? <AiOutlineUserAdd /> : <AiOutlineSave />}
                            type="submit"
                        >
                            {formType === 'register' ? 'Register' : 'Update'}
                        </Button>

                    </Group>
                )}
            </form>
        </Paper>
    )
}
export default Register