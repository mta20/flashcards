import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import { Provider } from 'react-redux';
// import reportWebVitals from './reportWebVitals';
import { MantineProvider } from '@mantine/core';
import { ModalsProvider } from '@mantine/modals';
import { NotificationsProvider } from '@mantine/notifications';
import store from "./redux/store";
ReactDOM.render(

  <Provider store={store}>
    <MantineProvider theme={{ fontFamily: 'Samim,gotham' }}>
      <NotificationsProvider>
        <ModalsProvider>

          <App />

        </ModalsProvider>
      </NotificationsProvider>
    </MantineProvider>
  </Provider>,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
// reportWebVitals();
